# GURPS Foundry Roll Library

A library for use in Foundry VTT modules and systems providing extendable, reusable functionality for making and displaying GURPS dice rolls.

Exposes classes like SuccessRoll and SuccessRollRenderer that can be used to generate and display GURPS dice rolls.

Use of the [https://gitlab.com/gurps-foundry/roll-templates](https://gitlab.com/gurps-foundry/roll-templates) Foundry module is required in order to render the rolls using default html templates. You are able to provide your own template in your system/module and pass it to the renderers, and I recommend doing so to avoid requiring end users to additionally install the aforementioned module. The easiest way is to simply copy the template from my module into yours.

## How to use
The classes within this library are exposed as ES6 modules. You will need to import them as such in your code.

### I use npm and Webpack or other similar tools
Just install the library with npm:
```
npm install git+https://gitlab.com/gurps-foundry/roll-lib.git#1.0.0
```
and you're good to go (don't forget to exchange 1.0.0 for whichever version of the library you wish to use)! You can import them in your code with for example:
```
import SuccessRoll from "gurps-foundry-roll-lib/src/js/Roll/SuccessRoll";
```

### I copy third-party libraries I need into my source tree
Just copy the `dist/gurps-foundry-roll-lib.js` file somewhere in your source tree and use relative path imports. For example, if your source tree looks like this:
```
libraries
    some_lib
    some_other_lib
main.js
module.json
```
You could copy the file like this:
```
libraries
    some_lib
    some_other_lib
    roll_lib
        gurps-foundry-roll-lib.js
main.js
module.json
```
In order to use the SuccessRoll class in your `main.js` file, you'd need to import it like this:
```
import { SuccessRoll } from "./libraries/roll_lib/gurps-foundry-roll-lib";
```