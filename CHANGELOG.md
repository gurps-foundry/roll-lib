# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.0] - 2020-10-25

The library is now compatible with Foundry VTT 0.7 while maintaining support for the 0.6 series. 

## [1.2.0] - 2020-10-24

### Added
- `DamageRoll` and `DamageRollRenderer` now support the same modifier description and `modList` functionality as was previously implemented for success rolls. The only difference is that modifier descriptions passed to `DamageRoll::fromFormula` can't start with a digit. Modifier descriptions passed through `DamageRoll::fromData` method may contain any characters.
- `ReactionRoll` and `ReactionRollRenderer` now support the same `modList` functionality as success and damage rolls.

## [1.1.1] - 2020-09-08

### Changed
- The `SuccessRoll::fromData` and `SuccessRoll::fromFormula` methods are now more robust in regards to accepting numeric parameteres; they now accept both integers and strings for all such values.
- `DamageRoll::fromFormula` now parses expressions using 'd6' instead of just 'd' properly. 
- `DamageRoll::fromFormula` no longer blows up when 0 is passed as the damage modifier.

## [1.1.0] - 2020-08-29

This version of the library requires the use of at least version 1.2.0 of the [GURPS Foundry Roll Templates](https://gitlab.com/gurps-foundry/roll-templates) module. 

### Added

#### Support for modifier description and structured modifier lists
The `SuccessRoll` class now supports modifier descriptions, and `SuccessRollRenderer` will produce output that includes mouseover tooltips over roll formulas. These tooltips display the list of modifiers passed to the roll along with their descriptions, while the roll formula will only display the modifiers themselves, as before. The tooltips are only rendered if at least one modifier passed to the roll has a description. This functionality can be accessed in multiple ways.

##### Structured modifier lists
A new `modList` argument can now be passed to `SuccessRoll::fromData`. It is an array containing a list of roll modifiers and their descriptions. The format is as follows:

```
[
    {
        modifier: '+4',
        description: 'All Out Attack (Determined)'
    },
    ...
]
```

If a `modList` is passed, it takes precedence over the `modifiers` argument, which is ignored.

Modifiers passed through `modList` will be evaluated and figured into the effective trait level the same as the `modifiers` argument.

##### Modifier descriptions in roll formula and modifiers expression

The roll formula passed to `SuccessRoll::fromFormula` and the `modifiers` argument passed to `SuccessRoll::fromData` now support modifier descriptions. Every modifier can now be followed by an optional description enclosed in parentheses, for example `+4 (All Out Attack Determined)`. A description passed this way may contain any characters except for parentheses.

## [1.0.0] - 2020-08-21
This marks the start of releasing the library using git tags and semantic versioning. I've been using semantic versioning in the package.json before, but without tagging that wasn't really relevant. I've also noticed that the library wasn't actually usable without npm, contrary to the readme and what I intended, so for this release I've dialled back the version in package.json back to 1.0.0.

### Features
- Classes for executing GURPS damage, success and reaction rolls
- API allows rolls defined by structured data as well as die roll expressions such as `1d+2(2) cut`.
- Classes for rendering rolls as Foundry VTT chat messages. This requires a customized Foundry roll template which can be passed to the renderer API as a configuration option. By default, the template from the [https://gitlab.com/gurps-foundry/roll-templates](https://gitlab.com/gurps-foundry/roll-templates) module is used and you will need to have that module installed if you don't want to provide your own template.  