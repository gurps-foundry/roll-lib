export const canonicizeModList = modList => modList.map(mod => {
    let modifier = parseInt(mod.modifier, 10);
    if (modifier > 0) {
        modifier = `+${modifier}`;
    } else if (modifier < 0) {
        modifier = `${modifier}`;
    }

    return {...mod, modifier};
});