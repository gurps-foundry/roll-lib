/**
 * Abstract class representing a dice roll to be extended for specific roll purposes.
 *
 * @abstract
 * @public
 * @hideconstructor
 */
export default class AbstractRoll {
    /**
     * @protected
     */
    constructor() {
        /**
         * @type {Roll}
         * @protected
         */
        this._foundryRoll = null;
    }

    /**
     * Evaluated roll formula expressed as XdY+Z
     *
     * @public
     * @type {string}
     */
    get evaluatedFormula() {
        return this._foundryRoll.formula;
    }

    /**
     * Roll result. Null if the roll hasn't been made yet.
     *
     * @public
     * @type {Number|null}
     */
    get total() {
        return this._foundryRoll.total;
    }

    /**
     * Foundry dice data
     *
     * @public
     * @type {Array.<Die>}
     */
    get foundryDice() {
        return this._foundryRoll.dice;
    }

    /**
     * Makes the roll
     *
     * @public
     * @returns {AbstractRoll} Self
     */
    roll() {
        this._foundryRoll.roll();
        return this;
    }
}
