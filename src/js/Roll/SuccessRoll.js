import AbstractRoll from './AbstractRoll.js';
import mexp from 'math-expression-evaluator';
import parse from './FormulaParser.js';
import { canonicizeModList } from './ModList.js';

/**
 * Class representing a success roll.
 *
 * @extends AbstractRoll
 * @hideconstructor
 * @inheritDoc
 * @public
 */
export default class SuccessRoll extends AbstractRoll {
    /**
     * Create a success roll object by parsing a formula based on the notation for skill levels published on sample characters in some GURPS supplements.
     *
     * @public
     * @param {String} formula Must include the target number to be rolled against. Can also optionally include the name of the ability being rolled against and any +/- modifiers. Accepted format is [&lt;ability name&gt;-]&lt;level&gt;[&lt;modifiers&gt;]. The ability name must start with a letter but can otherwise contain any character except line breaks and the - character, which if the ability name is passed must be used between the ability name and level. The optional list of modifiers may contain one or more modifiers supplied in +X or -Y form followed by an optional description in parentheses; optional spaces are allowed between the modifiers as well as between them and the +/- signs, but no other characters are allowed and every numeric modifier must have either a + or - sign preceding it. Optional modifier descriptions may contain any characters except parentheses.
     * @returns {SuccessRoll}
     *
     * @example
     * // Roll against 11
     * SuccessRoll.fromFormula('11')
     *
     * // Roll against 11 modified by -4 and +1
     * SuccessRoll.fromFormula('11-4+1')
     *
     * // Roll against the Stealth skill at level 12
     * SuccessRoll.fromFormula('Stealth-12')
     *
     * // Roll against ST 10 with a bonus of +3 and a penalty of -2 to the roll
     * SuccessRoll.fromFormula('ST-10 +3 -2')
     *
     * // Roll against Acrobatics at level 14 modified by +1 from Perfect Balance and -2 from medium encumbrance
     * SuccessRoll.fromFormula('Acrobatics-14 +1 (Perfect Balance) -2 (medium encumbrance)')
     */
    static fromFormula(formula) {
        return new SuccessRoll({ formula });
    }

    /**
     * Creates a success roll object from structured data.
     *
     * @public
     * @param {Object} data
     * @param {Number} data.level Level of the trait to roll against
     * @param {String|null} data.trait Name of the trait being rolled
     * @param {String|null} data.modifiers Various modifiers to apply to the roll supplied in +X or -Y form. Optional spaces are allowed between the modifiers as well as between them and the +/- signs, but no other characters are allowed and every numeric modifier must have either a + or - sign preceding it.
     * @param {Object[]} data.modList Array of modifier objects in the form of {modifier: '+1', description: 'Sneaky shoes'} allowing for structured management of modifiers. Overrides the 'modifiers' param.
     * @returns {SuccessRoll}
     *
     * @example
     * // Roll against the Stealth skill at level 12
     * SuccessRoll.fromData({ level: 12, trait: 'Stealth' })
     *
     * // Roll against ST 10 with a bonus of +3 and a penalty of -2 to the roll
     * SuccessRoll.fromData({ level: 10, trait: 'ST', modifiers: ' +3 -2' })
     *
     * // Roll against 11
     * SuccessRoll.fromData({ level: 11 })
     *
     * // Roll against 11 modified by -4 and +1
     * SuccessRoll.fromData({ level: 11, modifiers: '-4+1' })
     *
     * // Roll against Acrobatics at level 14 modified by +1 from Perfect Balance and -2 from medium encumbrance
     * SuccessRoll.fromData({ level: 14, trait: 'Acrobatics', modList: [{modifier: '+1', description: 'Perfect Balance'}, {modifier: '-2', description: 'medium encumbrance'}] })
     */
    static fromData({ level, trait = null, modifiers = null, modList = [] }) {
        return new SuccessRoll({ level, trait, modifiers, modList });
    }

    /**
     * @private
     * @param {Number} level Level of the trait to roll against
     * @param {String} trait Name of the trait being rolled
     * @param {String} modifiers Various +/- modifiers to apply to the roll
     * @param {Object[]} modList Array of modifier objects in the form of {modifier: '+1', description: 'Sneaky shoes'} allowing for structured management of modifiers. Overrides the 'modifiers' param.
     * @param {String} formula Success roll formula to be parsed, this can be passed instead of the previous four parameters (which it overrides completely)
     */
    constructor({ level = null, trait = null, formula = null, modifiers = null, modList = [] }) {
        super();

        if (formula === null && level === null) {
            throw new Error('Attempted to create a SuccessRoll with both level and formula equal to null; at least one of them must be non-null!');
        }

        // formula has highest priority, modList has precedence over modifiers, and modifiers are used only if none of the other two is
        if (formula !== null) {
            const parsedFormula = parse.successRollFormula(formula);
            trait = parsedFormula.trait;
            level = parsedFormula.level;
            modifiers = parsedFormula.modifiers;
            modList = modifiers !== null ? parse.modList(modifiers) : [];
        } else if (modList.length === 0) {
            modList = modifiers !== null ? parse.modList(modifiers) : [];
        }

        level = parseInt(level, 10);
        modList = canonicizeModList(modList);

        modList = modList.reverse();
        modifiers = modList.length > 0 ? modList.map(mod => mod.modifier).join(' ') : null;

        this._foundryRoll = new Roll('3d6');

        /**
         * @type {Number}
         * @protected
         */
        this._level = level;

        /**
         * @type {String|null}
         * @protected
         */
        this._trait = trait;

        /**
         * @type {String|null}
         * @protected
         */
        this._modifiers = modifiers;

        /**
         * @type {Array}
         * @protected
         */
        this._modList = modList;

        /**
         * @type {Number}
         * @protected
         */
        this._evaluatedModifiers = modList.reduce((sum, mod) => sum + mexp.eval((mod.modifier).toString()), 0);

        /**
         * @type {Boolean}
         * @protected
         */
        this._isSuccess = null;

        /**
         * @type {Number}
         * @protected
         */
        this._marginOfSuccess = null;

        /**
         * @type {Boolean}
         * @protected
         */
        this._isCritFail = false;

        /**
         * @type {Boolean}
         * @protected
         */
        this._isCritSuccess = false;
    }

    /**
     * Level of the trait being rolled against. This does not include any modifiers supplied to the roll.
     *
     * @public
     * @type {Number}
     */
    get level() {
        return this._level;
    }

    /**
     * Name of the trait being rolled against
     *
     * @public
     * @type {String|null}
     */
    get trait() {
        return this._trait;
    }

    /**
     * Modifiers applied to the success roll
     *
     * @public
     * @type {String|null}
     */
    get modifiers() {
        return this._modifiers;
    }

    /**
     * Array of modifiers applied to the success roll with their descriptions. Elements of the array are objects in the {modifier: '+1', description: 'some modifier'} format
     *
     * @public
     * @type {Object[]}
     */
    get modList() {
        return this._modList;
    }

    /**
     * Effective level of the trait being rolled against after taking into account all of the modifiers
     *
     * @public
     * @type {Number}
     */
    get evaluatedLevel() {
        return this.level + this._evaluatedModifiers;
    }

    /**
     * Null if the roll hasn't been made yet.
     *
     * @public
     * @type {boolean|null}
     */
    get isSuccess() {
        if (!this._foundryRoll._rolled) {
            return null;
        }

        return this._isSuccess;
    }

    /**
     * Null if the roll hasn't been made yet.
     *
     * @public
     * @type {Number|null}
     */
    get marginOfSuccess() {
        if (!this._foundryRoll._rolled) {
            return null;
        }

        return this._marginOfSuccess;
    }

    /**
     * Null if the roll hasn't been made yet.
     *
     * @public
     * @type {null|boolean}
     */
    get isCritFail() {
        if (!this._foundryRoll._rolled) {
            return null;
        }

        return this._isCritFail;
    }

    /**
     * Null if the roll hasn't been made yet.
     *
     * @public
     * @type {null|boolean}
     */
    get isCritSuccess() {
        if (!this._foundryRoll._rolled) {
            return null;
        }

        return this._isCritSuccess;
    }

    /**
     * Makes the roll
     *
     * @public
     * @returns {SuccessRoll} Self
     */
    roll() {
        super.roll();

        this._marginOfSuccess = this.level - this.total + this._evaluatedModifiers;

        this._isSuccess = this._marginOfSuccess >= 0;

        if (this.total <= 4 || (this.total <= 6 && this.marginOfSuccess >= 10)) {
            this._isCritSuccess = true;
            this._isSuccess = true;
        }

        if (this.total === 17) {
            this._isSuccess = false;
        }

        if (this.total === 18 || (this.total === 17 && this.level + this._evaluatedModifiers <= 15) || this.marginOfSuccess <= -10) {
            this._isCritFail = true;
            this._isSuccess = false;
        }

        return this;
    }
}
