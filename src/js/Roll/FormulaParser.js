export default class FormulaParser {
    static successRollFormula(formula) {
        const regexMatch = (formula + '').match(/^(?:([a-zA-Z][^-\r\n\f\v\0]*)-)?( *\d+)((?: *[+-] *[0-9]+ *(?: *\([^\r\n\f\v\0()]+\) *)?)*)$/);
        if (regexMatch === null) {
            throw new Error(
                `Can't parse success roll formula '${formula}' - it must adhere to the following format: [<trait name beginning with a letter and containing no - signs>-]<level>[<numeric modifiers including the + and - signs, and optionally spaces>]`
            );
        }

        return { trait: regexMatch[1] || null, level: parseInt(regexMatch[2], 10), modifiers: regexMatch[3] || null };
    }

    static modList(modifiersFormulaLeftToMatch) {
        const modList = [];

        while (modifiersFormulaLeftToMatch.length > 0) {
            const formulaMatch = modifiersFormulaLeftToMatch.match(/^( *[+-] *[0-9]+ *(?: *\([^\r\n\f\v\0()]+\) *)?)+$/);
            if (formulaMatch === null) {
                throw new Error(
                    `Can't parse modifier list formula '${modifiersFormulaLeftToMatch}' - it must adhere to the following format: <mandatory numeric modifier including the + or - sign>[<optional modifier description enclosed in parenthesis>], occurring one or more times`
                );
            }

            // formulaMatch[1] will contain the last modifier/description string present in modifiersFormulaLeftToMatch
            // singleModExpression will now be something like '+3 (foo bar)' or just '-2'
            const singleModExpression = formulaMatch[1];
            // we cut off that last mod/description string from the formula for the following iteration
            modifiersFormulaLeftToMatch = modifiersFormulaLeftToMatch.substr(0, modifiersFormulaLeftToMatch.length - singleModExpression.length);

            // singleModExpressionMatch will contain the numeric modifier at position 1, and a description at
            // position 2 if one was supplied; it it wasn't, position 2 will be undefined
            const singleModExpressionMatch = singleModExpression.trim().match(/^([+-] *[0-9]+) *(?: *\(([^\r\n\f\v\0()]+)\))?$/);

            modList.push({
                modifier: singleModExpressionMatch[1].replace(/\s/, ''),
                description: singleModExpressionMatch[2] !== undefined ? singleModExpressionMatch[2].trim() : '',
            });
        }

        return modList;
    }

    static damageRollFormula(formula) {
        const regexMatch = (formula + '').match(/^([1-9][0-9]*)d6?((?: *[+-] *[0-9]+ *(?: *\([^\d\r\n\f\v\0()][^\r\n\f\v\0()]*\) *)?)*)(.*)$/);
        if (regexMatch === null) {
            throw new Error(
                `Can't parse damage roll formula '${formula}' - it must adhere to the following format: <number of dice>d[<numeric modifiers including the + and - signs, and optionally spaces>][<extra non-roll information such as armor divisor, damage type etc>]`
            );
        }

        return { dice: parseInt(regexMatch[1], 10), adds: regexMatch[2] || '', meta: regexMatch[3] || '' };
    }
}
