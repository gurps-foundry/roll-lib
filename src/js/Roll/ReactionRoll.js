import AbstractRoll from './AbstractRoll.js';
import mexp from 'math-expression-evaluator';
import parse from './FormulaParser.js';
import { canonicizeModList } from './ModList';

/**
 * Class representing a reaction roll.
 *
 * @extends AbstractRoll
 * @hideconstructor
 * @inheritDoc
 * @public
 */
export default class ReactionRoll extends AbstractRoll {
    /**
     * Creates a reaction roll object from structured data.
     *
     * @public
     * @param {Object|null} data
     * @param {String|null} data.modifiers Reaction roll modifiers in a string format
     * @param {Object[]|null} data.modList Array of modifier objects in the form of {modifier: '+1', description: 'Appearance (Attractive)'} allowing for structured management of reaction modifiers. Overrides data.modifiers.
     * @returns {ReactionRoll}
     *
     * @example
     * // Plain reaction roll
     * ReactionRoll.fromData();
     *
     * // Reaction roll with a +5 bonus
     * ReactionRoll.fromData({ modifiers: '+5' });
     *
     * // Reaction roll with +2 from one source and -1 from another
     * ReactionRoll.fromData({ modifiers: '+2 -1' });
     *
     * // Reaction roll with -1 from Appearance (Unattractive) and +2 from Voice
     * ReactionRoll.fromData({ modList: [{modifier: '-1', description: 'Appearance (Unattractive)'}, {modifier: '+2', description: 'Voice'}] });
     */
    static fromData(data = null) {
        return new ReactionRoll({ modifiers: data ? data.modifiers || null : null, modList: data ? data.modList || [] : [] });
    }

    /**
     * @private
     * @param {Object} data
     * @param {String|null} data.modifiers Reaction roll modifiers
     * @param {Object[]} data.modList Array of modifier objects in the form of {modifier: '+1', description: 'Appearance (Attractive)'} allowing for structured management of reaction modifiers. Overrides data.modifiers.
     */
    constructor({ modifiers = null, modList = [] }) {
        super();

        /**
         * @type {Number}
         * @protected
         */
        this._evaluatedModifiers = null;

        if (modList.length > 0) {
            modList = canonicizeModList(modList);
            modifiers = modList.map(mod => mod.modifier).join(' ');
            this._evaluatedModifiers = mexp.eval(modifiers);
            this._foundryRoll = new Roll(`3d6+${this._evaluatedModifiers}`);
        } else if (modifiers) {
            const regexMatch = (modifiers + '').match(/^(( *[+-] *[0-9]+)*)/);
            if(regexMatch === null) {
                throw new Error(
                    `Can't parse ReactionRoll modifiers formula '${modifiers}' - it must comprise of numeric modifiers including the + and - signs, and optionally spaces between them`
                );
            }

            this._evaluatedModifiers = mexp.eval((regexMatch[1]).toString());
            this._foundryRoll = new Roll(`3d6+${this._evaluatedModifiers}`);

            modList = parse.modList(regexMatch[1]);
            modList = canonicizeModList(modList);
            modList = modList.reverse();
        } else {
            this._evaluatedModifiers = 0;
            this._foundryRoll = new Roll('3d6');
        }

        /**
         * @type {Object[]}
         * @protected
         */
        this._modList = modList;

        /**
         * @type {String|null}
         * @protected
         */
        this._modifiers = modifiers;

        /**
         * @type {String}
         * @protected
         */
        this._total = null;
    }

    /**
     * Array of modifiers applied to the success roll with their descriptions. Elements of the array are objects in the {modifier: '+1', description: 'some modifier'} format
     *
     * @public
     * @type {Object[]}
     */
    get modList() {
        return this._modList;
    }

    /**
     * Modifiers to the reaction roll as passed during roll creation
     *
     * @public
     * @type {String|null}
     */
    get modifiers() {
        return this._modifiers;
    }

    /**
     * Evaluated modifiers to the reaction roll
     *
     * @public
     * @type {Number}
     */
    get evaluatedModifiers() {
        return this._evaluatedModifiers;
    }

    /**
     * Null if the roll hasn't been made yet.
     *
     * @public
     * @type {String|null}
     */
    get total() {
        if (!this._foundryRoll._rolled) {
            return null;
        }

        return this._total;
    }

    /**
     * Makes the roll
     *
     * @public
     * @returns {ReactionRoll} Self
     */
    roll() {
        super.roll();

        this._total = 'Disastrous'

        if (this._foundryRoll.total >= 1) {
            this._total = 'Very Bad'
        }

        if (this._foundryRoll.total >= 4) {
            this._total = 'Bad'
        }

        if (this._foundryRoll.total >= 7) {
            this._total = 'Poor'
        }

        if (this._foundryRoll.total >= 10) {
            this._total = 'Neutral'
        }

        if (this._foundryRoll.total >= 13) {
            this._total = 'Good'
        }

        if (this._foundryRoll.total >= 16) {
            this._total = 'Very Good'
        }

        if (this._foundryRoll.total >= 19) {
            this._total = 'Excellent'
        }

        return this;
    }
}
