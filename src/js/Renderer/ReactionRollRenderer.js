import AbstractRollRenderer from './AbstractRollRenderer.js';

/**
 * Class used for rendering reaction rolls.
 *
 * @extends AbstractRollRenderer
 * @inheritDoc
 * @public
 */
export default class ReactionRollRenderer extends AbstractRollRenderer {
    /**
     * @protected
     * @param {ReactionRoll} roll
     * @returns {string|null}
     */
    _getFormula(roll) {
        if (roll.modifiers) {
            return `Reaction roll with ${roll.modifiers}<b style="padding-left: 10px">= ${roll.evaluatedModifiers}</b>`;
        }

        return 'Reaction roll';
    }

    /**
     * @protected
     * @param {ReactionRoll} roll
     * @returns {String}
     */
    _getTotal(roll) {
        return roll.total;
    }


    /**
     * @protected
     * @param {ReactionRoll} roll
     * @return {String} HTML representing the mouseover tooltip that is toggled when the roll formula in the rendered roll is hovered over with the mouse pointer.
     */
    _getMouseover(roll) {
        if (roll.modList.length === 0) {
            return null;
        }

        if (roll.modList.every(mod => mod.description === null || mod.description === undefined || mod.description.length === 0)) {
            return null;
        }

        const ulStyle = 'list-style-type: none; margin: 0; padding: 0;';
        const liStyle = 'margin: 0; padding: 0; line-height: 0.9rem;';

        return `<ul style="${ulStyle}"><li style="${liStyle}">3d base</li>${roll.modList.map(mod => `<li style="${liStyle}">${mod.modifier} ${mod.description}</li>`).join('')}</ul>`;
    }
}
