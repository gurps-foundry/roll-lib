import AbstractRollRenderer from './AbstractRollRenderer.js';
import apiVer from '../apiVersion.js';

/**
 * Class used for rendering success rolls.
 *
 * @extends AbstractRollRenderer
 * @inheritDoc
 * @public
 */
export default class SuccessRollRenderer extends AbstractRollRenderer {
    /**
     * Returns the face of the passed die that is considered the best possible result to be rolled. For success rolls, this is the lowest face.
     *
     * @protected
     * @param {Die} die
     * @returns {number}
     */
    _getBestDieFace(die) {
        return apiVer.isGreaterOrEqualTo07() ? 1 : Math.min(...die.sides);
    }

    /**
     * Returns the face of the passed die that is considered the worst possible result to be rolled. For success rolls, this is the highest face.
     *
     * @protected
     * @param {Die} die
     * @returns {number}
     */
    _getWorstDieFace(die) {
        return apiVer.isGreaterOrEqualTo07() ? die.faces : Math.max(...die.sides);
    }

    /**
     * @protected
     * @param {SuccessRoll} roll
     * @returns {string|null}
     */
    _getFormula(roll) {
        let formula = this._getFormulaWithoutMods(roll);
        if (roll.modifiers !== null) {
            formula = `${formula} ${roll.modifiers}<b> = ${roll.evaluatedLevel}</b>`;
        }

        return formula;
    }

    /**
     * @protected
     * @param {SuccessRoll} roll
     * @returns {string|null}
     */
    _getFormulaWithoutMods(roll) {
        const formula = roll.trait ? `${roll.trait}-` : 'Rolled against ';
        return `${formula}${roll.level}`;
    }

    /**
     * @param {SuccessRoll} roll
     * @returns {String}
     * @protected
     */
    _getTotal(roll) {
        let description = roll.isSuccess ? 'Success' : 'Failure';

        if (roll.isCritSuccess || roll.isCritFail) {
            const color = roll.isCritSuccess ? '#008800' : '#DD0000';
            description = `<b style="color:${color}">Critical ${description}</b>`;
        }

        return `${description} by ${roll.isSuccess ? roll.marginOfSuccess : -roll.marginOfSuccess}`;
    }

    // TODO default template with roll mods should look like this after 0.7 is released:
    // Stealth-12 +2 -1 =13
    // <mouseover classic tooltip>
    // Stealth-12
    // +2 very sneaky shoes
    // -1 rattling chain mail
    //
    // 3d6          7
    // 3 1 3
    // </mouseover classic tooltip>
    // <on-click Foundry tooltip that expands between Stealth-13 and Success/Failure by>
    // Stealth-12
    // +2 very sneaky shoes
    // -1 rattling chain mail
    //
    // 3d6          7
    // 3 1 3
    // </on-click Foundry tooltip>
    // Success by 6

    /**
     * @protected
     * @param {SuccessRoll} roll
     * @return {String} HTML representing the mouseover tooltip that is toggled when the roll formula in the rendered roll is hovered over with the mouse pointer.
     */
    _getMouseover(roll) {
        if (roll.modList.length === 0) {
            return null;
        }

        if (roll.modList.every(mod => mod.description === null || mod.description === undefined || mod.description.length === 0)) {
            return null;
        }

        const ulStyle = 'list-style-type: none; margin: 0; padding: 0;';
        const liStyle = 'margin: 0; padding: 0; line-height: 0.9rem;';

        return `<ul style="${ulStyle}"><li style="${liStyle}">${this._getFormulaWithoutMods(roll)}</li>${roll.modList.map(mod => `<li style="${liStyle}">${mod.modifier} ${mod.description}</li>`).join('')}</ul>`;
    }
}
