const api = {
    isGreaterOrEqualTo07: () => !isNewerVersion("0.7.0", game.data.version)
};

export default api;