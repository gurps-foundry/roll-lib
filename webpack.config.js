const path = require('path');
const EsmWebpackPlugin = require('@purtuga/esm-webpack-plugin');

module.exports = {
    entry: './src/js/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'gurps-foundry-roll-lib.js',
        library: 'GurpsFoundryRollLib',
        libraryTarget: 'var',
    },
    plugins: [new EsmWebpackPlugin()],
    devtool: 'inline-source-map'
};
